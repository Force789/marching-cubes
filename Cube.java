import java.util.HashMap;
import java.util.Map;

public class Cube {
	private Map<Integer,Arete> myMapArete;
	
	public Cube()
	{
		this.myMapArete = new HashMap<>();
	}
	
	public void addArete(int i,Arete a)
	{
		this.myMapArete.put(i, a);
	}
	
	public void remplirMap(Arete a_1, Arete a_2,Arete a_3 ,Arete a_4,Arete a_5, Arete a_6, Arete a_7, Arete a_8)
	{
		this.myMapArete.put(1, a_1);
		this.myMapArete.put(2, a_2);
		this.myMapArete.put(3, a_3);
		this.myMapArete.put(4, a_4);
		this.myMapArete.put(5, a_5);
		this.myMapArete.put(6, a_6);
		this.myMapArete.put(7, a_7);
		this.myMapArete.put(8, a_8);
	}
	
	public Arete rechercheArete_parID(int index)
	{
		return this.myMapArete.get(index);
	}

	
}
