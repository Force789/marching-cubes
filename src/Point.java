
public class Point {
	public final double X;
	public final double Y;
	public final double Z;
	
	public Point()
	{
		this.X = 0;
		this.Y = 0;
		this.Z = 0;
	}
	
	public Point(double myX, double myY, double myZ)
	{
		this.X = myX;
		this.Y = myY;
		this.Z = myZ;
	}
	
	
	@Override
	public String toString()
	{
		return (X + "," + Y + "," + Z);
	}
}
