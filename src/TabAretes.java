
public class TabAretes {
	private Arete []tabAretes;
	
	protected TabAretes()
	{
		this.tabAretes = new Arete[12];
		this.initTab();
	}
	
	protected void initTab()
	{
		this.tabAretes[0] = new Arete(0,1);
		this.tabAretes[1] = new Arete(1,2);
		this.tabAretes[2] = new Arete(2,3);
		this.tabAretes[3] = new Arete(3,0);
		this.tabAretes[4] = new Arete(0,4);
		this.tabAretes[5] = new Arete(1,5);
		this.tabAretes[6] = new Arete(2,6);
		this.tabAretes[7] = new Arete(3,7);
		this.tabAretes[8] = new Arete(4,5);
		this.tabAretes[9] = new Arete(5,6);
		this.tabAretes[10] = new Arete(6,7);
		this.tabAretes[11] = new Arete(7,4);
	}
	
}

