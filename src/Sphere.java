
public class Sphere extends Fonction {

	
	private final double xc;
	private final double yc;
	private final double zc;
	private final double r;
	
	public Sphere(double xc, double yc, double zc,double r)
	{
		this.xc = xc;
		this.yc = yc;
		this.zc = zc;
		this.r = r;
	}
	
	
	@Override
	public double eval(Point myPoint) {
		
		return Math.pow((myPoint.X - this.xc), 2) + Math.pow(myPoint.Y - this.yc, 2) + Math.pow(myPoint.Z - this.zc, 2) - Math.pow(this.r, 2);
	}

}
