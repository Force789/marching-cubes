
public class Triangle {
	public int pt1;
	public int pt2;
	public int pt3;
	
	public Triangle(int a, int b,int c) {
		this.pt1 = a;
		this.pt2 = b;
		this.pt3 = c;
	}
	@Override
	public String toString()
	{
		return "(Coordonnées du triangle :"+ this.pt1 +";"+ this.pt2 +";"+ this.pt3 +")";
	}
}
