
public class Arete {
	private int pt1;
	private int pt2;
	private int []arete;
	
	public Arete(int a, int b)
	{
		this.arete = new int[2];
		this.pt1 = a;
		this.pt2 = b;
		this.arete[0]=this.pt1;
		this.arete[1]=this.pt2;
	}	
	
	public int[] getArete()
	{
		return this.arete;
	}
}
