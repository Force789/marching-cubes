import java.util.ArrayList;

public class Config {
	public ArrayList<Triangle> myConfig;
	
	public Config()
	{
		this.myConfig = new ArrayList<Triangle>();
	}
	
	public void ajoutTriangle(int a, int b, int c)
	{
		this.myConfig.add(new Triangle(a,b,c));
	}
	
	public ArrayList<Triangle> getConfig()
	{
		return this.myConfig;
	}
	
	public int Nbtriangles()
	{
		return myConfig.size()/3;
	}

	
	@Override
	public String toString()
	{
		String temp = new String();
		for(Triangle C : myConfig)
		{
			temp.concat(C.toString());
		}
		return temp;
	}
}
