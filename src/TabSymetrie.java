public final class TabSymetrie {


	private TabSymetrie()
	{

	}


	public static int SymetrieXY(int numArete)					//On raisonne en aretes
	{
		int temp[] = new int[12];

		temp[0] = 2;
		temp[1] = 1;
		temp[2] = 0;
		temp[3] = 3;
		temp[4] = 7;
		temp[5] = 6;
		temp[6] = 5;
		temp[7] = 4;
		temp[8] = 10;
		temp[9] = 9;
		temp[10] = 8;
		temp[11] = 11;


		return temp[numArete];
	}



	public static int SymetrieXZ(int numArete)				//On raisonne en Aretes
	{
		int temp[] = new int[12];

		temp[0] = 8;
		temp[1] = 9;
		temp[2] = 10;
		temp[3] = 11;
		temp[4] = 7;
		temp[5] = 6;
		temp[6] = 5;
		temp[7] = 4;
		temp[8] = 0;
		temp[9] = 1;
		temp[10] = 2;
		temp[11] = 3;


		return temp[numArete];	
	}

	public static int SymetrieYZ(int numArete)				//On raisonne en Aretes
	{
		int temp[] = new int[12];

		temp[0] = 0;
		temp[1] = 3;
		temp[2] = 2;
		temp[3] = 1;
		temp[4] = 5;
		temp[5] = 4;
		temp[6] = 7;
		temp[7] = 6;
		temp[8] = 8;
		temp[9] = 11;
		temp[10] = 10;
		temp[11] = 9;


		return temp[numArete];	
	}

	public static int Rotation90X(int numArete)					//On raisonne en aretes
	{
		int temp[] = new int[12];

		temp[0] = 8;
		temp[1] = 5;
		temp[2] = 0;
		temp[3] = 4;
		temp[4] = 11;
		temp[5] = 9;
		temp[6] = 1;
		temp[7] = 3;
		temp[8] = 10;
		temp[9] = 6;
		temp[10] = 2;
		temp[11] = 7;


		return temp[numArete];	
	}

	public static int Rotation90Y(int numArete)					//On raisonne en aretes
	{
		int temp[] = new int[12];

		temp[0] = 3;
		temp[1] = 0;
		temp[2] = 1;
		temp[3] = 2;
		temp[4] = 7;
		temp[5] = 4;
		temp[6] = 5;
		temp[7] = 6;
		temp[8] = 11;
		temp[9] = 8;
		temp[10] = 9;
		temp[11] = 10;


		return temp[numArete];	
	}

	public static int Rotation90Z(int numArete)					//On raisonne en aretes
	{
		int temp[] = new int[12];

		temp[0] = 5;
		temp[1] = 8;
		temp[2] = 6;
		temp[3] = 1;
		temp[4] = 0;
		temp[5] = 8;
		temp[6] = 10;
		temp[7] = 6;
		temp[8] = 2;
		temp[9] = 11;
		temp[10] = 7;
		temp[11] = 3;


		return temp[numArete];	
	}

	public static int RotationM90Y(int numArete)					//On raisonne en aretes
	{
		int temp = numArete;

		for(int i = 0; i < 3 ; i++)
		{
			temp = Rotation90Y(temp);
		}

		return temp;

	}

	public static int RotationM90X(int numArete)					//On raisonne en aretes
	{
		int temp = numArete;

		for(int i = 0; i < 3 ; i++)
		{
			temp = Rotation90X(temp);
		}

		return temp;
	}

	public static int RotationM90Z(int numArete)					//On raisonne en aretes
	{
		int temp = numArete;

		for(int i = 0; i < 3 ; i++)
		{
			temp = Rotation90Z(temp);
		}

		return temp;
	}

	public static int ConfigSymetrieXY(int initConfig)				//On raisonne en Points
	{

		int temp = 0b11111111;
		int res;
		res = initConfig & temp;  									//on veille � bien avoir 8 bits

		int dep1D = 0b00100010;											//d points qui vont derriere le cube | g points qui vont devant (suivant la convention)
		int dep1G = 0b01000100;
		int dep3D = 0b00010001;
		int dep3G = 0b10001000;

		dep1D = initConfig & dep1D;
		dep1G = initConfig & dep1G;
		dep3D = initConfig & dep3D;
		dep3G = initConfig & dep3G;



		res = dep1D << 1 | dep1G >> 1 | dep3D << 3 | dep3G >> 3;


		return res;
	}



	public static int ConfigSymetrieXZ(int initConfig)				//On raisonne en Points
	{

		int temp = 0b11111111;
		int res;
		res = initConfig & temp;  									//on veille � bien avoir 8 bits


		temp = res & 0b11110000;
		res = res & 0b00001111;

		res = temp >> 4 | res << 4;


		return res;
	}

	public static int ConfigSymetrieYZ(int initConfig)				//On raisonne en Points
	{

		int temp = 0b11111111;
		int res;
		res = initConfig & temp;  									//on veille � bien avoir 8 bits


		temp = res & 0b01010101;
		res = res & 0b10101010;

		res = temp << 1 | res >> 1;


		return res;
	}


	public static int ConfigRotation90X(int initConfig)				//On raisonne en Points
	{

		int temp = 0b11111111;
		int temp1;
		int temp2;
		int temp3;
		int res;
		res = initConfig & temp;  									//on veille � bien avoir 8 bits


		temp = res & 0b00000011;
		temp1 = res & 0b00001100;
		temp2 = res & 0b00110000;
		temp3 = res & 0b11000000;

		res = temp << 4 | temp1 >> 2 | temp2 << 2 | temp3 >> 4;


		return res;
	}


	public static int ConfigRotation90Y(int initConfig)				//On raisonne en Points
	{

		int temp = 0b11111111;
		int temp1;
		int res;
		res = initConfig & temp;  									//on veille � bien avoir 8 bits


		temp = res & 0b01110111;
		temp1 = res & 0b10001000;


		res = temp << 1 | temp1 >> 3;


		return res;
	}

	public static int ConfigRotation90Z(int initConfig)
	{
		int temp = 0b11111111;
		int temp1;
		int temp2;
		int temp3;
		int res;
		res = initConfig & temp;  		

		temp = 0b01000001 & res;
		temp1 = 0b00101000 & res;
		temp2 = 0b00000110 & res;
		temp3 = 0b10010000 & res;

		res = temp << 1 | temp1 >> 1 | temp2 << 4 | temp3 >> 4; 


		return res;



	}

	public static int not(int initConfig)
	{
		int res = 0b11111111;
		res = res & ~initConfig;
		

		return res;



	}


	
	public static int ConfigRotationM90X(int initConfig)				//On raisonne en Points
	{

		int res = initConfig;
		
		for(int i = 0;i <3; i++)
		{
			res = ConfigRotation90X(res);
		}

		return res;
	}
	
	public static int ConfigRotationM90Y(int initConfig)				//On raisonne en Points
	{

		int res = initConfig;
		
		for(int i = 0;i <3; i++)
		{
			res = ConfigRotation90Y(res);
		}

		return res;
	}
	
	
	public static int ConfigRotationM90Z(int initConfig)				//On raisonne en Points
	{

		int res = initConfig;
		
		for(int i = 0;i < 3; i++)
		{
			res = ConfigRotation90Z(res);
		}

		return res;
	}






	public static int[] TabConfigSymetrieXY (int aTab[])
	{
		for (int i = 0; i < 16; i++)
		{
			aTab[i] = ConfigSymetrieXY(aTab[i]);
		}

		return aTab;
	}

	public static int[] TabConfigSymetrieXZ (int aTab[])
	{
		for (int i = 0; i < 16; i++)
		{
			aTab[i] = ConfigSymetrieXY(aTab[i]);
		}

		return aTab;
	}

	public static int[] TabConfigSymetrieYZ (int aTab[])
	{
		for (int i = 0; i < 16; i++)
		{
			aTab[i] = ConfigSymetrieYZ(aTab[i]);
		}

		return aTab;
	}


}
