import java.lang.Math; 

public class Grille {
	private final Point MIN_POINT;
	private final Point MAX_POINT;
	
	private static final double DEFAULT_STEP = 0.1d;
	
	private double step;
	
	public Grille(Point minPos, Point maxPos) 
	{
		this.MIN_POINT = minPos;
		this.MAX_POINT = maxPos;
		this.step = DEFAULT_STEP;
	}
		
	/* Boucle pour parcourir du point minim au point max avec le step */
	/* Remplacer system.out.println par la fonction qui renvoie une valeur*/
	
	public void evalAll(/*Fonction myFonction*/)
	{
		Point point;
		double i = this.MIN_POINT.X;
		double j = this.MIN_POINT.Y;
		double k = this.MIN_POINT.Z;
		
		for(i= this.MIN_POINT.X; i<this.MAX_POINT.X; i=i+this.step ){
			
			point= new Point(i,j,k);
			System.out.println(point.toString());
			
			point = new Point(i,j+this.step,k);
			System.out.println(point.toString());
			
			point = new Point(i,j,k+this.step);
			System.out.println(point.toString());
			
			point = new Point(i+this.step,j,k);
			System.out.println(point.toString());
			
			point = new Point(i+this.step,j+this.step,k);			
			System.out.println(point.toString());
			
			point = new Point(i+this.step,j,k+this.step);
			System.out.println(point.toString());
			
			point = new Point(i,j+this.step,k+this.step);
			System.out.println(point.toString());
			
			point = new Point(i+this.step,j+this.step,k+this.step);
			System.out.println(point.toString());
			
			/*************************/
			/* Mettre la fonction qui récup les valeurs*/
			/*************************/
			
		}
			
		for(j= this.MIN_POINT.Y; j<this.MAX_POINT.Y; j=j+this.step ){
			
			point= new Point(i,j,k);
			System.out.println(point.toString());
			
			point = new Point(i,j+this.step,k);
			System.out.println(point.toString());
			
			point = new Point(i,j,k+this.step);
			System.out.println(point.toString());
			
			point = new Point(i+this.step,j,k);
			System.out.println(point.toString());
			
			point = new Point(i+this.step,j+this.step,k);			
			System.out.println(point.toString());
			
			point = new Point(i+this.step,j,k+this.step);
			System.out.println(point.toString());
			
			point = new Point(i,j+this.step,k+this.step);
			System.out.println(point.toString());
			
			point = new Point(i+this.step,j+this.step,k+this.step);
			System.out.println(point.toString());
			
		}
		
		for(k= this.MIN_POINT.Z; k<this.MAX_POINT.Z; k=k+this.step ){
			
			point= new Point(i,j,k);
			System.out.println(point.toString());
			
			point = new Point(i,j+this.step,k);
			System.out.println(point.toString());
			
			point = new Point(i,j,k+this.step);
			System.out.println(point.toString());
			
			point = new Point(i+this.step,j,k);
			System.out.println(point.toString());
			
			point = new Point(i+this.step,j+this.step,k);			
			System.out.println(point.toString());
			
			point = new Point(i+this.step,j,k+this.step);
			System.out.println(point.toString());
			
			point = new Point(i,j+this.step,k+this.step);
			System.out.println(point.toString());
			
			point = new Point(i+this.step,j+this.step,k+this.step);
			System.out.println(point.toString());
			
		}
		
	}
	
	/*
	 * Fonction qui calcule le point d'intersection 
	 */
	public double evalDistance(Point p1, Point p2) {
		return Math.sqrt( (p2.X-p1.X)*(p2.X-p1.X) + (p2.Y-p1.Y)*(p2.Y-p1.Y) + (p2.Z-p1.Z)*(p2.Z-p1.Z) );
	}
	
	public Point evalIntersec(double dist, Point p1, Point p2)
	{
		double p_inter_X = ((dist*p1.X)-p2.X)/(dist-1);
		double p_inter_Y = ((dist*p1.Y)-p2.Y)/(dist-1);
		double p_inter_Z = ((dist*p1.Z)-p2.Z)/(dist-1);
		
		Point inter = new Point(p_inter_X, p_inter_Y, p_inter_Z);
		return inter;
	}
	
/*	public void calculPoint(Point p1, Point p2, Fonction myFonction) {
	
		Point p1_calc = myFonction.evalPoint(p1);
		Point p2_calc = myFonction.evalPoint(p2);
		
		double d1 = this.evalDistance(p2, p2_calc);
		double d2 = this.evalDistance(p1, p1_calc);
		
		double distance = d1/d2;
		
		Point intersec = this.evalIntersec(distance,p1,p2); 
		
		System.out.println(intersec.toString());
	}	*/
	
}
