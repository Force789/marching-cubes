public class TableauConfig {
	private Config tabconfig[];
	private int tabAretes[] = {0,1,2,3,4,5,6,7,8,9,10,11};
	private int tabNoConfig[] = {0b00000000,0b00000001,0b00000011,0b00100001,0b01000001,0b00001110,0b01000011,0b01010010,0b00001111,0b10001101,
			0b01010101,0b01001101,0b00011110,0b10100101,0b10001110,0b11111111};


	public TableauConfig()
	{		
		this.tabconfig = new Config[256];
		this.init15tab(tabconfig,tabAretes,tabNoConfig);
	}

	protected void init15tab(Config []tab, int tabAretes[], int tabNoConfig[] )
	{
		tab[tabNoConfig[0]] = null;
		tab[tabNoConfig[1]] = initconfig1(tabAretes);
		tab[tabNoConfig[2]] = initconfig2(tabAretes);
		tab[tabNoConfig[3]] = initconfig3(tabAretes);
		tab[tabNoConfig[4]] = initconfig4(tabAretes);
		tab[tabNoConfig[5]] = initconfig5(tabAretes);
		tab[tabNoConfig[6]] = initconfig6(tabAretes);
		tab[tabNoConfig[7]] = initconfig7(tabAretes);
		tab[tabNoConfig[8]] = initconfig8(tabAretes);
		tab[tabNoConfig[9]] = initconfig9(tabAretes);
		tab[tabNoConfig[10]] = initconfig10(tabAretes);
		tab[tabNoConfig[11]] = initconfig11(tabAretes);
		tab[tabNoConfig[12]] = initconfig12(tabAretes);
		tab[tabNoConfig[13]] = initconfig13(tabAretes);
		tab[tabNoConfig[14]] = initconfig14(tabAretes);
		tab[tabNoConfig[15]] = null;
	}

	/*	public void affichetab()							//Attention plus conforme vu que le tableau ne va plus de 0 � 15
	{
		for(int i=0; i<16; i++)
		{
			if(this.tabconfig[i] != null) {
				this.tabconfig[i].afficheConfig();
			}
		}
	}*/

	protected Config initconfig1(int tabAretes[])
	{
		Config myConfig = new Config();
		myConfig.ajoutTriangle(0,3,4);
		return myConfig;

	}

	protected Config initconfig2(int tabAretes[])
	{
		Config myConfig = new Config();
		myConfig.ajoutTriangle(1, 3, 5);
		myConfig.ajoutTriangle(5, 4, 3);
		return myConfig;
	}

	protected Config initconfig3(int tabAretes[])
	{
		Config myConfig = new Config();
		myConfig.ajoutTriangle(0, 3, 4);
		myConfig.ajoutTriangle(5, 8, 9);
		return myConfig;
	}

	protected Config initconfig4(int tabAretes[])
	{
		Config myConfig = new Config();
		myConfig.ajoutTriangle(0, 3, 4);
		myConfig.ajoutTriangle(6, 9, 10);
		return myConfig;
	}

	protected Config initconfig5(int tabAretes[])
	{
		Config myConfig = new Config();
		myConfig.ajoutTriangle(5,6,7);
		myConfig.ajoutTriangle(3,5,7);
		myConfig.ajoutTriangle(0,3,5);
		return myConfig;
	}

	protected Config initconfig6(int tabAretes[])
	{
		Config myConfig = new Config();
		myConfig.ajoutTriangle(1, 3, 5);
		myConfig.ajoutTriangle(5,4, 3);
		myConfig.ajoutTriangle(6, 9, 10);
		return myConfig;
	}

	protected Config initconfig7(int tabAretes[])
	{
		Config myConfig = new Config();
		myConfig.ajoutTriangle(4, 8,11);
		myConfig.ajoutTriangle(6, 9, 10);
		myConfig.ajoutTriangle(0, 1, 5);
		return myConfig;
	}

	protected Config initconfig8(int tabAretes[])
	{
		Config myConfig = new Config();
		myConfig.ajoutTriangle(4, 6, 7);
		myConfig.ajoutTriangle(4, 5, 6);
		return myConfig;
	}

	protected Config initconfig9(int tabAretes[])
	{
		Config myConfig = new Config();
		myConfig.ajoutTriangle(0, 4, 10);
		myConfig.ajoutTriangle(0, 6, 10);
		myConfig.ajoutTriangle(0, 1, 6);
		myConfig.ajoutTriangle(4, 10, 11);
		return myConfig;
	}

	protected Config initconfig10(int tabAretes[])
	{
		Config myConfig = new Config();
		myConfig.ajoutTriangle(0, 3, 8);
		myConfig.ajoutTriangle(3, 8, 11);
		myConfig.ajoutTriangle(1, 2, 9);
		myConfig.ajoutTriangle(3, 9, 10);
		return myConfig;
	}

	protected Config initconfig11(int tabAretes[])
	{
		Config myConfig = new Config();
		myConfig.ajoutTriangle(0, 4, 7);
		myConfig.ajoutTriangle(0, 1, 9);
		myConfig.ajoutTriangle(0, 7, 9);
		myConfig.ajoutTriangle(7, 9, 10);
		return myConfig;
	}

	protected Config initconfig12(int tabAretes[])
	{
		Config myConfig = new Config();
		myConfig.ajoutTriangle(4, 8, 11);
		myConfig.ajoutTriangle(3, 5, 7);
		myConfig.ajoutTriangle(0, 3, 5);
		myConfig.ajoutTriangle(5,6, 7);
		return myConfig;
	}

	protected Config initconfig13(int tabAretes[])
	{
		Config myConfig = new Config();
		myConfig.ajoutTriangle(0, 3, 4);
		myConfig.ajoutTriangle(2, 3, 6);
		myConfig.ajoutTriangle(5, 8, 9);
		myConfig.ajoutTriangle(7, 10, 11);
		return myConfig;
	}

	protected Config initconfig14(int tabAretes[])
	{
		Config myConfig = new Config();
		myConfig.ajoutTriangle(0, 3, 11);
		myConfig.ajoutTriangle(0, 6, 11);
		myConfig.ajoutTriangle(0, 1, 6);
		myConfig.ajoutTriangle(6, 10, 11);
		return myConfig;
	}




	protected void initAllConf()
	{
		Triangle temp;
		int pt1;
		int pt2;
		int pt3;


		for (int i = 1 ; i < 254; i++)
		{
			if (this.tabconfig[i] != null)
			{
				
				this.tabconfig[TabSymetrie.ConfigRotation90X(i)] = new Config();

				for (int j = 0; j < this.tabconfig[i].Nbtriangles(); j++)
				{
					temp = this.tabconfig[i].myConfig.get(j);

					pt1 = TabSymetrie.Rotation90X(temp.pt1);
					pt2 = TabSymetrie.Rotation90X(temp.pt2);
					pt3 = TabSymetrie.Rotation90X(temp.pt3);
					this.tabconfig[TabSymetrie.ConfigRotation90X(i)].ajoutTriangle(pt1, pt2, pt3);
				}
				
				this.tabconfig[TabSymetrie.ConfigRotation90Y(i)] = new Config();

				for (int j = 0; j < this.tabconfig[i].Nbtriangles(); j++)
				{
					temp = this.tabconfig[i].myConfig.get(j);

					pt1 = TabSymetrie.Rotation90Y(temp.pt1);
					pt2 = TabSymetrie.Rotation90Y(temp.pt2);
					pt3 = TabSymetrie.Rotation90Y(temp.pt3);
					this.tabconfig[TabSymetrie.ConfigRotation90Y(i)].ajoutTriangle(pt1, pt2, pt3);
				}

				this.tabconfig[TabSymetrie.ConfigRotation90Z(i)] = new Config();

				for (int j = 0; j < this.tabconfig[i].Nbtriangles(); j++)
				{
					temp = this.tabconfig[i].myConfig.get(j);

					pt1 = TabSymetrie.Rotation90Z(temp.pt1);
					pt2 = TabSymetrie.Rotation90Z(temp.pt2);
					pt3 = TabSymetrie.Rotation90Z(temp.pt3);
					this.tabconfig[TabSymetrie.ConfigRotation90Z(i)].ajoutTriangle(pt1, pt2, pt3);
				}

				
				
				
				
				
				
				
				
				
				
				
				
				this.tabconfig[TabSymetrie.not(TabSymetrie.ConfigRotationM90X(i))] = new Config();

				for (int j = 0; j < this.tabconfig[i].Nbtriangles(); j++)
				{
					temp = this.tabconfig[i].myConfig.get(j);

					pt1 = TabSymetrie.RotationM90X(temp.pt1);
					pt2 = TabSymetrie.RotationM90X(temp.pt2);
					pt3 = TabSymetrie.RotationM90X(temp.pt3);
					this.tabconfig[TabSymetrie.not(TabSymetrie.ConfigRotationM90X(i))].ajoutTriangle(pt1, pt2, pt3);
				}
				
				this.tabconfig[TabSymetrie.not(TabSymetrie.ConfigRotationM90Y(i))] = new Config();

				for (int j = 0; j < this.tabconfig[i].Nbtriangles(); j++)
				{
					temp = this.tabconfig[i].myConfig.get(j);

					pt1 = TabSymetrie.RotationM90Y(temp.pt1);
					pt2 = TabSymetrie.RotationM90Y(temp.pt2);
					pt3 = TabSymetrie.RotationM90Y(temp.pt3);
					this.tabconfig[TabSymetrie.not(TabSymetrie.ConfigRotationM90Y(i))].ajoutTriangle(pt1, pt2, pt3);
				}

				this.tabconfig[TabSymetrie.not(TabSymetrie.ConfigRotationM90Z(i))] = new Config();

				for (int j = 0; j < this.tabconfig[i].Nbtriangles(); j++)
				{
					temp = this.tabconfig[i].myConfig.get(j);

					pt1 = TabSymetrie.RotationM90Z(temp.pt1);
					pt2 = TabSymetrie.RotationM90Z(temp.pt2);
					pt3 = TabSymetrie.RotationM90Z(temp.pt3);
					this.tabconfig[TabSymetrie.not(TabSymetrie.ConfigRotationM90Z(i))].ajoutTriangle(pt1, pt2, pt3);
				}
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				this.tabconfig[TabSymetrie.ConfigSymetrieXY(i)] = new Config();

				for (int j = 0; j < this.tabconfig[i].Nbtriangles(); j++)
				{
					temp = this.tabconfig[i].myConfig.get(j);

					pt1 = TabSymetrie.SymetrieXY(temp.pt1);
					pt2 = TabSymetrie.SymetrieXY(temp.pt2);
					pt3 = TabSymetrie.SymetrieXY(temp.pt3);
					this.tabconfig[TabSymetrie.ConfigSymetrieXY(i)].ajoutTriangle(pt1, pt2, pt3);
				}


				this.tabconfig[TabSymetrie.ConfigSymetrieXZ(i)] = new Config();

				for (int j = 0; j < this.tabconfig[i].Nbtriangles(); j++)
				{
					temp = this.tabconfig[i].myConfig.get(j);

					pt1 = TabSymetrie.SymetrieXZ(temp.pt1);
					pt2 = TabSymetrie.SymetrieXZ(temp.pt2);
					pt3 = TabSymetrie.SymetrieXZ(temp.pt3);
					this.tabconfig[TabSymetrie.ConfigSymetrieXZ(i)].ajoutTriangle(pt1, pt2, pt3);
				}

				this.tabconfig[TabSymetrie.ConfigSymetrieYZ(i)] = new Config();

				for (int j = 0; j < this.tabconfig[i].Nbtriangles(); j++)
				{
					temp = this.tabconfig[i].myConfig.get(j);

					pt1 = TabSymetrie.SymetrieYZ(temp.pt1);
					pt2 = TabSymetrie.SymetrieYZ(temp.pt2);
					pt3 = TabSymetrie.SymetrieYZ(temp.pt3);
					this.tabconfig[TabSymetrie.ConfigSymetrieYZ(i)].ajoutTriangle(pt1, pt2, pt3);
				}
				
			}
		}
	}


	public int nbEvalConf()
	{
		int temp = 0;
		for(int i=0; i < this.tabconfig.length; i++)
		{
			if(this.tabconfig[i] != null)
			{
				temp +=1;
			}

		}

		return temp;
	}

}